#!/usr/bin/python3
# -*- coding: utf-8 -*-

class Persona:
    def __init__(self, n, d):
        self.nombre = n 
        self.dni = d

class Empleado(Persona):
   """Un ejemplo de clase para Empleados"""
   def __init__(self, n, d, s):
       super().__init__(n, d)
       self.nomina = s
       if s<0 :
            self.nomina = 0
   def calcula_impuestos (self):
       return self.nomina*0.30
   def __str__(self):
       return "El empleado {name} debe pagar {tax:.2f}".format(name=self.nombre, tax=self.calcula_impuestos())

class Jefe(Empleado):
    def __init__(self, n, d, s, extra=0):
        super().__init__(n, d, s)
        self.bonus = extra
    def calcula_impuestos1 (self):
        return super().calcula_impuestos() + self.bonus*0.30
    def __str__(self):
        return "El jefe {name} debe pagar pagar {tax:.2f}".format(name=self.nombre, tax=self.calcula_impuestos())
 
