import unittest
from empleado import Empleado
from jefe import Jefe

class TestEmpleado(unittest.TestCase):
   def test_construir(self):
       e1 = Empleado("nombre", "dni", 5000)
       self.assertEqual(e1.nomina, 5000)    
   def test_impuestos(self):
       e1 = Empleado("nombre", "dni", 5000)
       self.assertEqual(e1.calcula_impuestos(), 1500)
   def test_str(self):
       e1 = Empleado("pepe", "dni", 50000)
       self.assertEqual("El empleado pepe debe pagar 15000.00", e1.__str__())
if __name__ == "__main__":
   unittest.main() 